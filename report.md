# Clustering

### How to use
* In Ulysses in `D1` directory
```
$ module load openblas
$ make  
$ make run
```
### Output for linear correlation on Leaf dataset
```
 class                -0.25151868453980053     
 Entropy              -0.13677152254930980     
 AveContrast          -0.13625091665346811     
 Max Indent D         -0.13170463958676315     
 AveIntensity         -0.13019691598311317     
 Smoothness           -0.12565652618636114     
 Third Moment         -0.11504653784967905     
 Uniformity           -0.10490322435605302     
 Elongation            -9.8622702662513717E-002
 Specimen Num          -8.3512880335963408E-002
 Lobedness             -8.0216471275071921E-002
 Aspect Ratio          -5.0635622425945832E-002
 Eccentricity           4.2079784571878658E-002
 Stoch Conver           7.7649468336763852E-002
 Solidity              0.10945130845549736     
 Isoperimetr           0.19470133459094799   
 ```
 ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/linear_correlation.png)
 
 ### Output for PCA on ionosphere dataset
  ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/pca.png)
   ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/pca_log.png)
 ### Output for Mutual Information on Votiing Dataset
![](https://github.com/rjtkp/Clustering/blob/master/D1/data/mutual_info.png)

 ### Output for K-means with random initialization on S3
 ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/kmeans.png)
  ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/starting_on_point.png)
 ### Output for K-means++ on S3
![](https://github.com/rjtkp/Clustering/blob/master/D1/data/km++.png)
 ### Output for K-means++ on S3 scree plot
![](https://github.com/rjtkp/Clustering/blob/master/D1/data/scree_plot.png)
 ### Output for C-means on S3
![](https://github.com/rjtkp/Clustering/blob/master/D1/data/cmeans_k15.png)
 ### Output for Density Peak on aggregation
![](https://github.com/rjtkp/Clustering/blob/master/D1/data/dp_aggre.png)
 ### Output for Density Peak on S3 dataset
 ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/dp_s3.png)
  ### Output for K-means on S3 dataset
 ![](https://github.com/rjtkp/Clustering/blob/master/D1/data/dp_aggre.png)
  
