PROGRAM k_means
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 2,column_file = 5000, nmax = 100!, K = 15!, nmax = 100 !,sp = 4
  REAL (KIND= dp), ALLOCATABLE ::dist(:), centers(:,:), p(:), dat(:,:)
  INTEGER(dp), ALLOCATABLE :: z(:)
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  REAL (KIND= dp):: x,  obj_fun, dm, obj(20)
  INTEGER (KIND = dp )::i, j, l, min_index(1), n, m, k, ii, it

  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(z(column_file))
  ALLOCATE(p(column_file))

  OPEN(unit=120,file='./data/out_put_kmpp_scree',status='unknown')

  READ(5,*) dat
  DO k = 2, 20, 1
     DO ii = 1, 20, 1

        ALLOCATE(dist(k))
        ALLOCATE(centers(row_file,k))

        obj_fun = 0.0d0

        !randomly innitalizing the centers of the cluster
        CALL random_NUMBER(x)
        centers(1,1) = dat(1,INT(x*column_file))
        centers(2,1) = dat(2,INT(x*column_file))

        DO m = 2, K, 1
           p(1) = 0.0d0
           ! we have to calculate the minimum probability which os
           !proportional to the distance square

           DO i=1,column_file
              DO j=1,K
                 dist(j) = NORM2(centers(:,j) - dat(:,i))
              ENDDO
              dm = MINVAL(dist, dim=1)
              p(i+1) = p(i) + dm**2
           ENDDO

           CALL RANDOM_NUMBER(x)
           x = x * p(column_file+1)
           l = 1

           DO WHILE (x > p(l+1))
              l = l + 1
           ENDDO
           centers(1,m) = dat(1, l)
           centers(2,m) = dat(2, l)
        END DO

        ! 2. Repeat until no instance changes its cluster membership:
        !- Decide the cluster membership of instances by assigning them
        ! to the nearest cluster centroid
        n = 1
        it = 1
        DO WHILE (n < 2 .AND. it <= nmax)
           n = 2

           ! Minimize intra distance

           DO i=1,column_file
              DO j=1,K
                 dist(j) = NORM2(centers(:,j)- dat(:,i))
              ENDDO
              min_index = MINLOC(dist)
              obj_fun = obj_fun + dist(min_index(1))
              IF ( z(i) /= min_index(1) ) THEN
                 z(i) = min_index(1)
                 n = 1
              ENDIF
           ENDDO

           ! Update the k cluster centroids based on the assigned cluster
           ! membership
           ! Maximize inter distance
           CALL centroid_update(dat,z,centers)
           it = it + 1
        END DO

        ! DO i = 1, column_file, 1
        !    WRITE(120,*) dat(1,i), dat(2,i), z(i)
        ! END DO

        DEALLOCATE(dist)
        DEALLOCATE(centers)

        OBJ(ii) = obj_fun
        ! WRITE(*,*) obj_fun

     END DO

     ! WRITE(120,*) k, SUM(obj)/20
  END DO


  CLOSE(120)

  DEALLOCATE(dat)
  DEALLOCATE(z)
  DEALLOCATE(p)

END PROGRAM k_means
