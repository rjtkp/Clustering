MODULE utilities
  IMPLICIT NONE
  EXTERNAL DSYEV
  PRIVATE

  INTEGER, PARAMETER :: sp = 4, dp = 8
  PUBLIC :: average, linear_correlation, sorted_features, bubble_sort,&
       event_counter, mutual_event, pca, covariant_mat, dist_mahalanobis,&
       hamming_dist, euclidean, centroid_update,&
       distance_update, update_u, ASSIGN, nearest_peak, & !, euclidean_distance
       peak_distance, probab, norm_mutual_info, f_ratio

CONTAINS

  SUBROUTINE average(g, mean)
    IMPLICIT NONE
    REAL(kind = dp), INTENT(INOUT) :: g(:), mean
    INTEGER:: i, j
    mean = 0.d0
    j = SIZE(g, dim=1)
    DO i = 1, j, 1
       mean = mean + g(i)
    END DO
    mean = mean/j
  END SUBROUTINE average


  SUBROUTINE   linear_correlation(x,y,corr)
    IMPLICIT NONE

    REAL(dp), INTENT(INOUT) :: x(:), y(:), corr
    INTEGER :: n,i
    REAL(dp) :: mx, my, acc1, acc2

    n = SIZE(x, dim=1)
    CALL average(x, mx)
    CALL average(y, my)
    corr = 0.d0
    acc1=0
    acc2=0

    DO i=1,n,1
       acc1 = acc1+(x(i)-mx)*(x(i)-mx)
       acc2 = acc2+(y(i)-my)*(y(i)-my)
       corr = corr + (x(i)-mx)*(y(i)-my)
    END DO

    corr = corr/SQRT(acc1*acc2)

  END SUBROUTINE  linear_correlation

  SUBROUTINE bubble_sort(arr) !RESULT(arr)
    IMPLICIT NONE
    REAL(dp), INTENT(inout):: arr(:)
    INTEGER(dp):: j, bubble, en
    REAL(dp) :: temp
    en = SIZE(arr)
    DO WHILE (en > 1)
       bubble = 0 !bubble in the greatest element out of order
       DO j = 1, (en-1)
          IF (arr(j) > arr(j+1)) THEN
             temp = arr(j)
             arr(j) = arr(j+1)
             arr(j+1) = temp
             bubble = j
          ENDIF
       ENDDO
       en = bubble
    ENDDO
  END SUBROUTINE bubble_sort

  SUBROUTINE sorted_features(corr, dat1)
    IMPLICIT NONE

    REAL(dp),DIMENSION(:), INTENT(INOUT) :: corr
    INTEGER(dp),DIMENSION(:), INTENT(INOUT) :: dat1
    INTEGER :: n, i, j
    REAL(dp) ::  tolerance
    ! INTEGER(dp),ALLOCATABLE :: dat1(:)
    REAL(dp), ALLOCATABLE :: dat(:)

    tolerance=1E-06
    n=SIZE(corr,1)
    ALLOCATE(dat(n))
    dat(:) = corr(:)
    CALL bubble_sort(corr)

    DO i=1,n
       DO j=1,n
          IF(ABS(corr(i)-dat(j)) < tolerance) THEN
             dat1(i)=j
          END IF
       END DO
    END DO

    DEALLOCATE(dat)

  END SUBROUTINE sorted_features

  !mutual_event caount total number of occurances happened
  !in given situations in given spaces
  INTEGER FUNCTION mutual_event(x, y, a, b)
    IMPLICIT NONE

    INTEGER(dp), INTENT(INOUT) :: x(:), y(:)
    INTEGER (dp),INTENT(IN) :: a
    INTEGER (sp),INTENT(IN) :: b
    INTEGER (dp):: i, n

    mutual_event=0
    n = SIZE(x, dim=1)
    DO i=1,n
       IF((x(i) == a) .AND. (y(i) == b)) THEN
          mutual_event = mutual_event + 1
       END IF
    END DO
  END FUNCTION mutual_event

  !It counts the number of events occured with a given
  !particular conditional space
  INTEGER FUNCTION event_counter(x,val)
    IMPLICIT NONE

    INTEGER(dp), DIMENSION(:), INTENT(in) :: x
    INTEGER(dp) :: i, n
    INTEGER (dp),INTENT(in) ::val
    event_counter=0
    n  = SIZE(x, dim=1)

    DO i=1,n
       IF(x(i)==val) THEN
          event_counter = event_counter + 1
       END IF
    END DO

  END FUNCTION event_counter

  SUBROUTINE covariant_mat(dat, cov)
    IMPLICIT NONE

    REAL(dp), INTENT(inout) :: dat(:,:), cov(:,:)
    REAL(dp) :: mi, mj
    INTEGER(dp) :: i, j, k, row, col

    col=SIZE(dat,1)
    row=SIZE(dat,2)

    cov(:,:) = 0.d0

    DO i=1,col,1
       CALL average(dat(i,:), mj)
       DO j=1,col
          CALL average(dat(j,:), mi)
          DO k=1,row
             cov(j,i)=cov(j,i)+(dat(j,k)-mi)*(dat(i,k)-mj)/row
          END DO
       END DO
    END DO

  END SUBROUTINE covariant_mat


  SUBROUTINE PCA(eigen_val, eigen_vect, dat)
    IMPLICIT NONE

    EXTERNAL DSYEV

    REAL(dp),INTENT(inout):: dat(:,:), eigen_val(:), eigen_vect(:,:)
    INTEGER(sp), PARAMETER:: LWMAX = 1000
    REAL(dp):: WORK(LWMAX)
    INTEGER(dp):: row
    INTEGER::info, LWORK

    CALL covariant_mat(dat, eigen_vect)
    row = SIZE(eigen_val, dim=1)

    LWORK = -1
    CALL DSYEV('Vectors','Upper',row, eigen_vect,row,eigen_val,WORK,LWORK,info)
    LWORK=MIN(LWMAX,INT(WORK(1)))

    CALL DSYEV('Vectors','Upper',row,eigen_vect,row,eigen_val,WORK,LWORK,info)

    IF( INFO.GT.0 ) THEN
       WRITE(*,*)'The algorithm failed to compute eigenvalues.'
       STOP
    END IF

  END SUBROUTINE PCA

  SUBROUTINE dist_mahalanobis(dat, dist)
    IMPLICIT NONE

    EXTERNAL DGETRI
    EXTERNAL DGEMV

    REAL(dp),INTENT(inout) :: dat(:,:), dist(:,:)
    INTEGER(sp), PARAMETER:: LWMAX = 1000
    INTEGER,ALLOCATABLE:: IPIV(:)
    REAL(dp)::DDOT, WORK(LWMAX)
    INTEGER::info, LWORK
    REAL(dp),ALLOCATABLE:: cov(:,:), dx(:), y(:)
    INTEGER:: i, j, row, col

    row = SIZE(dat,1)
    col = SIZE(dat,2)

    ALLOCATE(cov(row, row))
    ALLOCATE(dx(row))
    ALLOCATE(y(row))
    ALLOCATE(IPIV(row))

    CALL covariant_mat(dat, cov)

    LWORK = -1
    CALL DGETRF(row, row, cov, row, IPIV, INFO )

    IF ( INFO /= 0 ) THEN
       WRITE(*,*) " An error occured. DGETRF INFO:", INFO
    ENDIF

    CALL DGETRI( row, cov, row, IPIV, WORK, LWORK, INFO)

    IF ( INFO /= 0 ) THEN
       WRITE(*,*) " An error occured. DGETRI INFO:", INFO
    ENDIF


    ! Computing the dist array here
    DO j=1,col
       dist(j,j) = 0.0
       DO i=j+1,col
          dx = dat(:,i) - dat(:,j)
          CALL DGEMV( 'N', row, row, 1.0d0, cov, row, dx, 1, 0.0d0, y, 1)

          !computes the inner product of two Vectors
          dist(i,j) = DDOT( row, dx, 1, y, 1)
          dist(j,i) = dist(i,j) !as the matrix will be symmetric
       END DO
    END DO

    DEALLOCATE(dx)
    DEALLOCATE(y)
    DEALLOCATE(cov)
    DEALLOCATE(IPIV)

  END SUBROUTINE dist_mahalanobis

  SUBROUTINE euclidean(dat, dist)
    IMPLICIT NONE

    REAL(dp),INTENT(inout) :: dat(:,:), dist(:,:)
    REAL(dp):: sum
    INTEGER:: i, j, col

    col = SIZE(dat,2)

    DO j=1,col
       dist(j,j) = 0.0
       DO i=j+1,col
          sum = 0.0d0
          ! DO k=1,col
          !    sum = sum + ())**2
          ! ENDDO
          dist(i,j) = NORM2(dat(:,i) -  dat(:,j))
          dist(j,i) = dist(i,j)
       END DO
    END DO

  END SUBROUTINE euclidean

  SUBROUTINE hamming_dist(dat, dist)
    IMPLICIT NONE

    REAL(dp),INTENT(inout) ::  dist(:,:)
    INTEGER(dp),INTENT(inout):: dat(:,:)
    REAL(dp):: sum
    INTEGER:: i, j, col, row, k


    row = SIZE(dat, dim=1)
    col = SIZE(dat, dim=2)


    DO j=1,col
       dist(j,j) = 0.0
       DO i=j+1,col
          sum = 0.0d0
          DO k=1,row
             sum = sum + sum_bits(IOR(dat(k,i), dat(k,j))) - sum_bits(IAND(dat(k,i), dat(k,j)))
          ENDDO
          dist(i,j) = sum          ! SQRT(sum)
          dist(j,i) = dist(i,j)
       END DO
    END DO

  END SUBROUTINE hamming_dist

  INTEGER FUNCTION sum_bits(i)
    INTEGER(dp) :: i
    INTEGER :: pos
    sum_bits = 0
    DO pos=0,63,1
       IF ( BTEST(i, pos) ) THEN
          sum_bits = sum_bits + 1
       END IF
    END DO
  END FUNCTION sum_bits

  SUBROUTINE centroid_update(dat, z, centers)
    IMPLICIT NONE

    REAL(dp),INTENT(inout):: dat(:,:), centers(:,:)
    INTEGER(dp),INTENT(inout):: z(:)
    INTEGER(dp):: i, j, col, n, k

    k = SIZE(centers, 2)
    col = SIZE(dat, 2)

    DO i=1,K
       n = 0
       centers(:,i) = 0.0d0
       DO j=1,col
          IF ( z(j) == i ) THEN
             n = n + 1
             centers(:,i) = centers(:,i) + dat(:,j)
          ENDIF
       ENDDO
       centers(:,i) = centers(:,i) / n
    ENDDO
  END SUBROUTINE centroid_update

  SUBROUTINE distance_update(dat, z, d, center_index)
    IMPLICIT NONE

    REAL(dp),INTENT(inout):: dat(:,:),d(:)
    INTEGER(dp),INTENT(inout):: z(:), center_index(:)
    REAL(dp):: dx
    INTEGER(dp):: i, j, col

    col = SIZE(dat, dim=2)


    DO i=1,col
       DO j=i,col
          IF ( z(i) == z(j) ) THEN
             dx = NORM2(dat(:,j) - dat(:,i))
             d(i) = d(i) + dx
             d(j) = d(i) + dx
          ENDIF
       ENDDO
    ENDDO

    DO i=1,col
       IF ( d(i) < d( center_index( z(i) ) ) ) THEN
          center_index(z(i)) = i
       ENDIF
    ENDDO

  END SUBROUTINE distance_update

  SUBROUTINE update_u(dat, centers, m, U, dist)
    IMPLICIT NONE

    REAL(dp),INTENT(inout):: dat(:,:), dist(:), centers(:,:), U(:,:)
    INTEGER(dp), INTENT(inout) :: m
    REAL(dp):: sum
    INTEGER(dp):: i, j, l, col, k


    col = SIZE(dat, dim=2)
    k = SIZE(centers, dim=2)

    DO i=1,col

       DO j=1,K
          dist(j) = NORM2(centers(:,j) - dat(:,i))
       ENDDO
       !
       DO j=1,K
          sum = 0.0d0
          DO l=1,K
             sum = sum + (dist(j) / dist(l))**(2/(m-1))
          ENDDO
          U(i,j) = 1.0d0 / sum
       ENDDO
    ENDDO
  END SUBROUTINE update_u

  SUBROUTINE ASSIGN(U,z)
    IMPLICIT NONE
    REAL(dp), INTENT(INOUT) ::u(:,:)
    INTEGER(dp), INTENT(INOUT) :: z(:)
    INTEGER(dp):: i,j,k, col

    col = SIZE(U, dim=1)
    k = SIZE(U, dim=2)

    DO i=1,col
       DO j=1,K
          IF ( U(i,j) > U(i, z(i))) THEN
             z(i) = j;
          ENDIF
       ENDDO
    ENDDO

  END SUBROUTINE ASSIGN

  SUBROUTINE peak_distance(den, dist, dc, dmax)
    IMPLICIT NONE
    REAL(dp), INTENT(INOUT) ::dist(:,:), den(:), dc, dmax
    ! INTEGER(dp), INTENT(INOUT) :: z(:)
    INTEGER(dp):: i,j, col

    col = SIZE(dist, dim=1)

    DO i=1,col
       DO j=1,col
          den(i) = den(i) + EXP(- (dist(i,j)/ dc)**2 )
       ENDDO
    ENDDO

    dmax=0.0d0
    DO i=1,col
       DO j=i+1,col
          IF (dmax < dist(i,j)) THEN
             dmax = dist(i,j)
          ENDIF
       ENDDO
    ENDDO

  END SUBROUTINE peak_distance

  SUBROUTINE nearest_peak(dist, den,peaks, peak_dist)
    IMPLICIT NONE
    REAL(dp), INTENT(INOUT) ::dist(:,:), den(:), peak_dist(:)
    INTEGER(dp), INTENT(INOUT) :: peaks(:)
    INTEGER(dp):: i,j, col

    col = SIZE(dist, dim=1)

    DO i=1,col
       DO j=1,col
          IF ( den(j) > den(i) ) THEN
             IF ( peak_dist(i) > dist(i,j) ) THEN
                peak_dist(i) = dist(i,j)
                peaks(i) = j
             ENDIF
          ENDIF
       ENDDO
    ENDDO
  END SUBROUTINE nearest_peak


  SUBROUTINE norm_mutual_info(mutual_info, z1, z2, k)
    IMPLICIT NONE

    REAL(dp), INTENT(INOUT) ::mutual_info, z1(:), z2(:)
    INTEGER(dp), INTENT(INOUT) :: k
    INTEGER(dp):: i,j, col, p1(k), p2(k), p(k,k)
    REAL(dp) :: nu, denu, x

    col = SIZE(z1, dim=1)

    nu=0.0d0
    denu = 0.0d0
    x = 0.d0
    p(:,:) = 0
    p1(:) = 0
    p2(:) = 0

    ! counting for the probability for events
    DO i=1,col
       p1(INT(z1(i))) = p1(INT(z1(i))) + 1
       p2(INT(z2(i))) = p2(INT(z2(i))) + 1
       p(INT(z1(i)),INT(z2(i))) = p(INT(z1(i)),INT(z2(i))) + 1
    END DO

    ! WRITE(*,*)  p1, p2

    DO i=1,k
       DO j=1,k
          IF (p(i,j) /= 0 .AND. p2(i) /= 0 .AND. p1(j) /= 0 ) THEN
             nu = nu + REAL(p(i,j)) * LOG( REAL(col * p(i,j)) / REAL(p2(i) * p1(j)) ) / REAL(col)
          ENDIF
       END DO
    END DO
    ! WRITE(*,*)  nu

    CALL probab(p2,col,denu)
    ! WRITE(*,*) denu
    CALL probab(p1,col,x)
    ! WRITE(*,*) x

    mutual_info = nu*2  / (denu+x)

  END SUBROUTINE norm_mutual_info

  SUBROUTINE probab(p, col, mutual_info)
    IMPLICIT NONE

    INTEGER(dp), INTENT(inout) :: col, p(:)
    REAL(dp), INTENT(inout):: mutual_info
    REAL(8) :: x
    INTEGER(dp):: i, n
    n = SIZE(p, dim=1)

    mutual_info = 0.0d0
    ! WRITE(*,*)  p
    x = 0.0d0
    DO i=1,n
       IF ( p(i) /= 0 ) THEN
          x = x - REAL(p(i)) * LOG( REAL(p(i)) / REAL(col) ) / REAL(col)
       ENDIF
    END DO

    mutual_info = mutual_info + x

  END SUBROUTINE probab

  SUBROUTINE f_ratio(ratio, dat,k)
    IMPLICIT NONE

    REAL(dp), INTENT(INOUT) ::dat(:,:), ratio
    INTEGER(dp), INTENT(INOUT) :: K
    REAL(dp), ALLOCATABLE:: centers(:,:), m(:)
    INTEGER(dp):: i,j, col, row, sizes(K)
    REAL(dp):: x, y
    sizes(:) = 0

    row = SIZE(dat,dim = 1) - 1
    ALLOCATE(m(row))
    ALLOCATE(centers(row, K))


    col = SIZE(dat,dim = 2)

    x = 0.0d0
    y = 0.0d0
    centers(:,:) = 0

    ! counting number of points in the clusters
    DO i=1,col
       DO j = 1, row, 1
          centers(j,INT(dat(row+1,i))) = centers(j,INT(dat(row+1,i))) + dat(j,i)
          sizes(INT(dat(row+1,i))) = sizes(INT(dat(row+1,i))) + 1
       END DO
    ENDDO
    DO i=1,K
       centers(:,i) = centers(:,i) / sizes(i)
    ENDDO
    DO i=1,col
       DO j = 1, row, 1
          x = x + ((dat(j,i) - centers(j,INT(dat(row+1,i))))**2)
       END DO
    ENDDO

    m = SUM(dat,2)
    m = m / row

    DO i=1,K
       DO j = 1, row, 1
          y = y + sizes(i) * ((centers(j,i) - m(j))**2)
       END DO
    ENDDO


    x = x * K
    ratio = x / y

    DEALLOCATE(centers)
    DEALLOCATE(m)

  END SUBROUTINE f_ratio

END MODULE utilities
