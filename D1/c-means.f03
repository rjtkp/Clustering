PROGRAM k_means
  USE utilities

  IMPLICIT NONE

  INTEGER(KIND=8), PARAMETER :: dp = 8, row_file = 2,column_file = 5000, nmax =100, m =2, K = 15!,sp = 4
  REAL (KIND= dp), ALLOCATABLE ::dist(:), centers(:,:), dat(:,:), U(:,:), tmp_U(:,:)
  INTEGER(dp), ALLOCATABLE :: z(:)
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  REAL (KIND= dp):: x,  obj_fun, tolerance, du, sum
  INTEGER (KIND = dp )::i, j, n, it, l

  ALLOCATE(z(column_file))
  ALLOCATE(dat(row_file, column_file))

  OPEN(unit=120,file='./data/out_put_cmeans_k15',status='unknown')

  READ(5,*) dat

  !Imortant initializations
  obj_fun = 0.0d0
  tolerance = 1e-4
  du = 5.0d0
  z(:) = 1
  it =1

  ALLOCATE(dist(k))
  ALLOCATE(U(column_file,k))
  ALLOCATE(tmp_U(column_file,k))
  ALLOCATE(centers(row_file,k))


  CALL random_SEED()

  DO i=1,column_file
     sum = 0.0d0
     DO l=1,K
        CALL RANDOM_NUMBER(x)
        U(i,l) = DBLE(x)
        sum = sum + U(i,l)
     ENDDO
     U(i,:) = U(i,:) / sum
  ENDDO
  !Until it converges keep updating our clusters
  DO WHILE ( tolerance < dU .AND. it <= nmax )
     tmp_U = U
     U = U**m
     DO l=1,K
        sum = 0.0d0
        DO i=1,column_file
           sum = sum + U(i,l)
        ENDDO
        U(:,l) = U(:,l) / sum
     ENDDO
     CALL DGEMM ('N', 'N', row_file, K, column_file, 1.0d0, dat, row_file, U, column_file, 0.0d0, centers, row_file)

     n = m
     !updating the u matrix
     CALL update_u(dat,centers,n,u,dist)

     DO i = 1, column_file, 1
        DO j = 1, k, 1
           obj_fun = obj_fun + (U(i,j)**m) * (dist(j)**2)
        END DO
     END DO

     dU = NORM2( tmp_U - U )
     it = it + 1
     obj_fun = 0.0d0
  ENDDO

  !this assigns the clusters to the numbered cluster in the system
  CALL ASSIGN(u,z)
  DO i = 1, column_file, 1
     WRITE (120,*) dat(1,i),dat(2,i),z(i)
  END DO

  DEALLOCATE(dat)
  DEALLOCATE(centers)
  DEALLOCATE(dist)
  DEALLOCATE(tmp_U)
  DEALLOCATE(U)
  DEALLOCATE(z)

END PROGRAM k_means
