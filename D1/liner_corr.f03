PROGRAM LINEAR_CORR
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 16,column_file = 340!, sp = 4
  REAL (KIND= dp), ALLOCATABLE :: dat(:,:), corr(:), a(:), dist(:,:)
  INTEGER(dp), ALLOCATABLE :: feature(:)
  CHARACTER(len=20), ALLOCATABLE :: labels(:)

  ! REAL (KIND= dp):: checksum , my_sum, tolerance, x ,y ! the real is 4byte long but has been convert to double presicion type with kind =8
  ! REAL (KIND= dp):: y_sum, x_sum, x2_sum, y2_sum, de, nu, r(16)
  INTEGER (KIND = dp )::  i, j, n


  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(dist(column_file, column_file))
  ALLOCATE(corr(row_file))
  ALLOCATE(feature(row_file))
  ALLOCATE(a(column_file))
  ALLOCATE(labels(row_file))
  OPEN(unit=120,file='./data/out_put',status='unknown')
  OPEN(unit=200,file='./data/distances_leaf',status='unknown')

  n = row_file*column_file

  corr(:) = 0.0

  READ(5,*) dat          ! so, read them into the array t

  a(:) = dat(:,1)

  DO i = 1, row_file, 1
     CALL linear_correlation(a, dat(i,:), corr(i))
  END DO

  labels = (/"class       ","Specimen Num", "Eccentricity", "Aspect Ratio", "Elongation  ",&
       "Solidity    ", "Stoch Conver", "Isoperimetr ", "Max Indent D",&
       "Lobedness   ", "AveIntensity", "AveContrast ", "Smoothness  ",&
       "Third Moment", "Uniformity  ", "Entropy     "/)


  feature(:) = 0

  CALL sorted_features(corr, feature)

  DO i = 1, row_file, 1
     WRITE(120,*)  labels(feature(i)),corr(i)
  END DO

  CALL dist_mahalanobis(dat,dist)

  DO i = 1, column_file, 1
     DO j = 1, column_file, 1
        WRITE(200,*)dist(j,i)
     END DO
  END DO

  CLOSE(unit=120)
  CLOSE(200)
  DEALLOCATE(dat)
  DEALLOCATE(corr)
  DEALLOCATE(a)
  DEALLOCATE(dist)
  DEALLOCATE(feature)
  DEALLOCATE(labels)


END PROGRAM LINEAR_CORR
