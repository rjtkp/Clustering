PROGRAM k_means
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 3,column_file = 788, K = 7!, nmax = 100 !,sp = 4
  REAL (KIND= dp), ALLOCATABLE :: dat(:,:)
  INTEGER(dp), ALLOCATABLE :: z(:)
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  REAL (KIND= dp):: centers(row_file,K), mn1, mx1, mn2, x, mx2, dist(k), obj_fun
  INTEGER (KIND = dp )::i,j, min_index(1), n

  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(z(column_file))

  OPEN(unit=120,file='./data/out_put_km_aggre',status='unknown')

  READ(5,*) dat

  obj_fun = 0.0d0

  !randomly innitalizing the centers of the cluster somewhere
  !random in the spaces
  mn1 = MINVAL(dat(1,:))
  mx1 = MAXVAL(dat(1,:))
  mn2 = MINVAL(dat(2,:))
  mx2 = MAXVAL(dat(2,:))  !

  ! DO i = 1, K, 1
  !    CALL random_NUMBER(x)
  !    centers(1,i) = x*(mx1 - mn1)
  !    centers(2,i) = x*(mx2 - mn2)
  ! END DO

  !randomly innitalizing the centers of the cluster to another
  !points of the in data
  DO i = 1, K, 1
     CALL random_NUMBER(x)
     centers(1,i) = dat(1,INT(x*column_file))
     centers(2,i) = dat(2,INT(x*column_file))
  END DO


  ! 2. Repeat until no instance changes its cluster membership:
  !- Decide the cluster membership of instances by assigning them
  ! to the nearest cluster centroid
  n = 1
  DO WHILE (n < 2)
     n = 2

     ! Minimize intra distance

     DO i=1,column_file
        DO j=1,K
           dist(j) = NORM2(centers(:,j) - dat(:,i))
        ENDDO
        min_index = MINLOC(dist)
        obj_fun = obj_fun + dist(min_index(1))
        IF ( z(i) /= min_index(1) ) THEN
           z(i) = min_index(1)
           n = 1
        ENDIF
     ENDDO

     ! Update the k cluster centroids based on the assigned cluster
     ! membership
     ! Maximize inter distance

     CALL centroid_update(dat,z,centers)
  END DO

  DO i = 1, column_file, 1
     WRITE(120,*) dat(1,i), dat(2,i), dat(3,i), z(i)
  END DO


  CLOSE(120)

  DEALLOCATE(dat)
  DEALLOCATE(z)

END PROGRAM k_means
