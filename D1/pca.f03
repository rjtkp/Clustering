PROGRAM pca_ranking
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 34,column_file = 351 !sp = 4,
  REAL (KIND= dp), ALLOCATABLE :: dist(:,:),new_dat(:,:),dat(:,:), eigen_val(:), eigen_vect(:,:)
  ! INTEGER(dp), ALLOCATABLE ::
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  INTEGER (KIND = dp )::  i, j

  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(dist(column_file, column_file))
  ALLOCATE(eigen_val(row_file))
  ALLOCATE(eigen_vect(row_file,row_file))

  OPEN(unit=120,file='./data/out_put_ion',status='unknown')
  OPEN(unit=180,file='./data/out_put_log_ion',status='unknown')
  OPEN(unit=200,file='./data/new_dat_ion',status='unknown')
  OPEN(unit=220,file='./data/distances_pca',status='unknown')

  READ(5,*) dat

  CALL pca(eigen_val, eigen_vect, dat)

  DO i = 1, row_file, 1
     WRITE(180,*) i, LOG(eigen_val(i))
  END DO


  eigen_val(:) = eigen_val(:)/SUM(eigen_val)

  DO i = 1, row_file, 1
     WRITE(120,*) i, eigen_val(i)
  END DO

  ALLOCATE(new_dat(7, column_file))

  CALL DGEMM ('N','N', 7.0d0, column_file, row_file, 1.0d0, eigen_vect, row_file, dat, row_file, 0.0d0, new_dat, 7.0d0)

  DO i = 1, 7, 1
     DO j = 1, column_file,1
        WRITE(200,*) new_dat(i,j)
     END DO
  END DO

  CALL euclidean(dat, dist)

  DO i = 1, column_file, 1
     DO j = 1, column_file, 1
        WRITE(220,*)dist(j,i)
     END DO
  END DO


  CLOSE(120)
  CLOSE(180)
  CLOSE(200)
  CLOSE(220)

  DEALLOCATE(dat)
  DEALLOCATE(dist)
  DEALLOCATE(eigen_vect)
  DEALLOCATE(eigen_val)
  DEALLOCATE(new_dat)

END PROGRAM PCA_ranking
