PROGRAM mutual_info
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 3,column_file = 788, K = 7!, nmax = 100!!, nmax = 100 !,sp = 4
  REAL (KIND= dp), ALLOCATABLE ::dat1(:,:),dat2(:,:), z1(:), z2(:)
  REAL(dp) :: norm_mut_info
  INTEGER(dp):: i
  ALLOCATE(z1( column_file ))
  ALLOCATE(z2( column_file ))
  ALLOCATE(dat1( row_file,column_file ))
  ALLOCATE(dat2( row_file,column_file ))

  READ(5,*) dat1, dat2

  z1(:) = dat1(3,:)
  z2(:) = dat2(3,:)

  DEALLOCATE(dat1)
  DEALLOCATE(dat2)
  ! DO i = 1, column_file, 1
  !    WRITE(*,*) z1, z2
  ! END DO
  WRITE(*,*) INT(MAXVAL(z1)), INT(MAXVAL(z2))
  i = k
  IF ( INT(MAXVAL(z1)) == k .AND. INT(MAXVAL(z2)) == k ) THEN
     CALL norm_mutual_info(norm_mut_info, z1, z2, i)
  ELSE
     WRITE(*,*) 'there is a mismatch between the cluster-numbers'
  END IF

  WRITE(*,*) "The normalized mutual info on aggregation between kmeans and density peak was found to be", norm_mut_info
  ! DO i = 1, 100, 1
  !    WRITE(*,*)  z1(i), z2(i)
  ! END DO



  DEALLOCATE(z1)
  DEALLOCATE(z2)
END PROGRAM mutual_info
