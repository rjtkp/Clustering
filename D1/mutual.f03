PROGRAM mutual_info
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 17,column_file = 435!, sp = 4
  REAL (KIND= dp), ALLOCATABLE ::  mut_info(:), dist(:,:)
  INTEGER(dp), ALLOCATABLE :: a(:), dat(:,:), feature(:)
  CHARACTER(len=200), ALLOCATABLE :: labels(:)
  INTEGER (KIND = dp )::  i, j, k, n, demo, rep, tmp

  ALLOCATE(labels(row_file))
  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(feature(row_file))
  ALLOCATE(mut_info(row_file))
  ALLOCATE(a(column_file))
  ALLOCATE(dist(column_file, column_file))


  OPEN(unit=120,file='./data/out_put_mut_info',status='unknown')
  OPEN(unit=220,file='./data/distances_votes',status='unknown')

  labels = (/  "            ","handicpd-inf", &
       "watersharing", &
       "adoption-res",&
       "fee-freeze  ",&
       "el-sal-aid  ", &
       "grp-inschool", &
       "antitest-ban", &
       "aid-to-nicar", &
       "mx-missile  ", &
       "immigration ", &
       "corp-cutback", &
       "edu-spending", &
       "right-to-sue", &
       "crime       ", &
       "dty-freeexpt", &
       "admin-act-SA"/)

  READ(5,*) dat
  n = column_file
  a(:) = dat(1,:)
  demo = SUM(a)
  rep = column_file - demo

  DO i=2,17,1
     mut_info(i-1) = 0

     DO j=1,3
        k=j-2
        tmp = event_counter(dat(i,:), k)
        mut_info(i-1) = mut_info(i-1) + 1.d0/n*mutual_event(dat(i,:),a,k,1)*LOG(n*1.d0*mutual_event(dat(i,:),a,k,1)/&
             (1.d0*demo*tmp)) + 1.d0/n*mutual_event(dat(i,:),a,k,0)*LOG(n*1.d0*mutual_event(dat(i,:),a,k,0)/(1.d0*rep*tmp))
     END DO

  END DO

  mut_info(1) = -1000
  CALL sorted_features(mut_info,feature)

  DO i = 2, row_file, 1
     WRITE(120,*) labels(feature(i)), mut_info(i)
  END DO

  CALL hamming_dist(dat,dist)

  DO i = 1, column_file, 1
     DO j = 1, column_file, 1
        WRITE(220,*)dist(j,i)
     END DO
  END DO


  CLOSE(120)
  CLOSE(220)

  DEALLOCATE(dist)
  DEALLOCATE(dat)
  DEALLOCATE(mut_info)
  DEALLOCATE(a)
  DEALLOCATE(feature)
  DEALLOCATE(labels)


END PROGRAM MUTUAL_INFO
