PROGRAM pca_ranking
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 2,column_file = 5000 !sp = 4,
  REAL (KIND= dp), ALLOCATABLE :: dat(:,:), eigen_val(:), eigen_vect(:,:)
  ! INTEGER(dp), ALLOCATABLE ::
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  INTEGER (KIND = dp )::  i

  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(eigen_val(row_file))
  ALLOCATE(eigen_vect(row_file,row_file))

  OPEN(unit=120,file='./data/out_put_ion',status='unknown')
  OPEN(unit=180,file='./data/out_put_log_ion',status='unknown')

  READ(5,*) dat

  CALL pca(eigen_val, eigen_vect, dat)

  DO i = 1, row_file, 1
     WRITE(180,*) i, LOG(eigen_val(i))
  END DO


  eigen_val(:) = eigen_val(:)/SUM(eigen_val)

  DO i = 1, row_file, 1
     WRITE(120,*) i, eigen_val(i)
  END DO

  CLOSE(120)
  CLOSE(180)

  DEALLOCATE(dat)
  DEALLOCATE(eigen_vect)
  DEALLOCATE(eigen_val)

END PROGRAM PCA_ranking
