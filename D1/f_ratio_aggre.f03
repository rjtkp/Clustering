PROGRAM Fratio
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 4,column_file = 788, K = 7!, nmax = 100!!, nmax = 100 !,sp = 4
  REAL (KIND= dp), ALLOCATABLE ::dat(:,:)
  REAL(dp) :: ratio
  INTEGER(dp):: i
  ALLOCATE(dat( row_file,column_file ))

  ! READ(*,*) k
  READ(5,*) dat

  ! DO i = 1, column_file, 1
  !    WRITE(*,*) z1, z2
  ! END DO
  ! WRITE(*,*) INT(MAXVAL(dat(3,:)))
  i = k
  WRITE(*,*) k
  IF ( INT(MAXVAL(dat(3,:))) == k  ) THEN
     CALL f_ratio(ratio, dat, i)
  ELSE
     WRITE(*,*) 'there is a mismatch between the cluster-numbers'
  END IF

  WRITE(*,*) "The f-ratio was found to be", ratio
  ! DO i = 1, 100, 1
  !    WRITE(*,*)  z1(i), z2(i)
  ! END DO



  DEALLOCATE(dat)

END PROGRAM FRATIO
