PROGRAM k_means
  USE utilities

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = 8, row_file = 2,column_file = 5000, K = 15!, nmax = 100!!, nmax = 100 !,sp = 4
  REAL(dp), PARAMETER :: dc = 57500.0
  REAL (KIND= dp), ALLOCATABLE ::assin(:,:),dist(:,:), den(:), dat(:,:), peak_dist(:)
  INTEGER(dp), ALLOCATABLE :: z(:), peaks(:)
  ! CHARACTER(len=200), ALLOCATABLE :: labels(:)
  REAL (KIND= dp):: dm, dmax, tmp
  INTEGER (KIND = dp )::i, j, ii,last, num

  ALLOCATE(dat(row_file, column_file))
  ALLOCATE(z(column_file))
  ALLOCATE(dist(column_file,column_file))
  ALLOCATE(den(column_file))
  ALLOCATE(peaks(column_file))
  ALLOCATE(peak_dist(column_file))
  ALLOCATE(assin(column_file,2))

  OPEN(unit=120,file='./data/out_put_dp_s3',status='unknown')

  dm = dc
  den(:) = 0.0d0
  dist(:,:) = 0.0d0

  READ(5,*) dat

  CALL euclidean(dat ,dist)
  dm = dc
  CALL peak_distance(den, dist, dm, dmax)

  peaks(:) = 0
  peak_dist = dmax

  CALL nearest_peak(dist, den,peaks, peak_dist)

  DO i=1,column_file
     assin(i, 1) = i
     assin(i, 2) = den(i) * peak_dist(i)
  ENDDO

  ii =2
  num = column_file
  last = num
  DO WHILE ( last /= 1 )
     last = 1
     DO i=1,num-1
        IF (assin(i,ii) < assin(i+1, ii)) THEN
           DO j=1,2
              tmp = assin(i, j)
              assin(i, j) = assin(i+1, j)
              assin(i+1, j) = tmp
           ENDDO
           num = i + 1
           last = i + 1
        END IF
     END DO
  END DO

  z(:) = 0
  DO i=1,K
     z(INT(assin(i, 1))) = i
  ENDDO
  DO i=1,column_file
     j = i
     DO WHILE (z(j) == 0 .AND. peaks(j) /= 0 )
        j = peaks(j)
     ENDDO
     z(i) = z(j)
  ENDDO

  DO i=1,column_file
     WRITE (120,*) dat(1,i), dat(2,i), z(i)
  ENDDO
  CLOSE(120)

  DEALLOCATE(dist)
  DEALLOCATE(dat)
  DEALLOCATE(peaks)
  DEALLOCATE(den)
  DEALLOCATE(z)
  DEALLOCATE(peak_dist)


END PROGRAM k_means
