#/bin/bash
awk 'BEGIN{ FS = ","; OFS = " "} {printf ("%d",($1=="democrat"? 1 : 2));\
for(i=2; i<=NF; i++){ if($i=="y") printf("\t%d",1);\
if($i=="n") printf("\t%d",-1);if($i=="?") printf("\t%d",0) } \
printf("\n") }' votes-84.data
