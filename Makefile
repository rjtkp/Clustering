# -*- Makefile -*-
SHELL=/bin/sh
############################################
# derived makefile variables
OBJ_SERIAL=$(SRC:src/%.f90=Obj-serial/%.o)
############################################

default: run

run:
	$(MAKE) $(MFLAGS) -C D1 default

clean:
	$(MAKE) $(MFLAGS) -C D1 clean
